from setuptools import setup

setup(
    name='django-dynamic-constance',
    version='0.1dev',
    packages=['dynamic_settings'],
    install_requires=['django-constance>=0.6']
)