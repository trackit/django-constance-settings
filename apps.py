from django.apps import apps, AppConfig
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _

import importlib

class DynamicSettingsConfig(AppConfig):
    name = 'dynamic_settings'
    verbose_name = _('Dynamic settings')

    def ready(self):
        self.dynamic_settings = {}
        for mod in apps.app_configs.keys( ):
            try:
                pkg = apps.get_app_package(mod)
                st = self._load_dynamic_settings(pkg)
                if st: self.dynamic_settings.update(st)
            except ImproperlyConfigured:
                pass

    def get_constance_settings_dict(self):
        return self.dynamic_settings

    def _load_dynamic_settings(self, pkg):
        dsets = {}
        try:
            s = importlib.import_module('.dynamic_settings', pkg)
            for i in dir(s):
                if i[0] != '_':
                    stl = getattr(s, i)
                    if isinstance(stl, (list, tuple)):
                        st = stl
                    elif isinstance(stl, (str, int, float, bool, list, tuple)):
                        st = (stl, _(
                            'Please supply help text for this setting.'))
                    else:
                        st = None
                    if st: dsets[i] = st
            return dsets
        except ImportError as iex:
            pass
